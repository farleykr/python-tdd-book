from django.http import HttpRequest
from django.test import TestCase
from django.urls import resolve

from lists.views import index_page


class IndexPageTest(TestCase):

    def test_root_url_resolves_to_index_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, index_page)

    def test_index_page_returns_correct_html(self):
        request = HttpRequest()
        response = index_page(request)
        html = response.content.decode('utf-8')
        self.assertTrue(html.startswith('<html>'))
        self.assertIn('<title>To-Do Lists</title>', html)
        self.assertTrue(html.endswith('</html>'))
        